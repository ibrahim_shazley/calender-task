USE [master]
GO
/****** Object:  Database [EvensDB]    Script Date: 29/05/2019 10:20:56 م ******/
CREATE DATABASE [EvensDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EvensDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\EvensDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EvensDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\EvensDB_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EvensDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EvensDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EvensDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EvensDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EvensDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EvensDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EvensDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [EvensDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EvensDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EvensDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EvensDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EvensDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EvensDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EvensDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EvensDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EvensDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EvensDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EvensDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EvensDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EvensDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EvensDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EvensDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EvensDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EvensDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EvensDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EvensDB] SET  MULTI_USER 
GO
ALTER DATABASE [EvensDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EvensDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EvensDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EvensDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EvensDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EvensDB]
GO
/****** Object:  Table [dbo].[CNT]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CNT](
	[id] [int] NOT NULL,
 CONSTRAINT [PK_CNT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Events]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[startDate] [nvarchar](300) NULL,
	[startTime] [nvarchar](300) NULL,
	[endDate] [nvarchar](300) NULL,
	[endTime] [nvarchar](300) NULL,
 CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[AddEvents]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddEvents]
@title NVARCHAR(50),
@start_date NVARCHAR(300),
@start_time NVARCHAR(300),
@end_date NVARCHAR(300),
@end_time NVARCHAR(300)
AS
BEGIN
	INSERT INTO dbo.Events
	        ( Title ,
	          startDate ,
	          startTime ,
	          endDate ,
	          endTime
	        )
	VALUES  ( @title , -- Title - nvarchar(50)
	          @start_date , -- startDate - nvarchar(300)
	          @start_time , -- startTime - nvarchar(300)
	          @end_date , -- endDate - nvarchar(300)
	          @end_time  -- endTime - nvarchar(300)
	        )
	      
			  SELECT 1 AS Id
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteEvent]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteEvent]
 @id int
AS
BEGIN
	DELETE FROM dbo.Events WHERE @id=ID
END

GO
/****** Object:  StoredProcedure [dbo].[EditEvent]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditEvent]
	@id INT,
	@title NVARCHAR(50),
	@start_date NVARCHAR(300),
	@start_time NVARCHAR(300),
	@end_date NVARCHAR(300),
	@end_time NVARCHAR(300)
AS
BEGIN
UPDATE dbo.Events SET Title=@title ,startDate=@start_date,startTime=@start_time ,[endDate]=@end_date ,endTime=@end_time WHERE ID=@id 
SELECT 1 AS id

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllEvents]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllEvents]
	
AS
BEGIN
	SELECT * FROM dbo.Events
END

GO
/****** Object:  StoredProcedure [dbo].[GetEventByID]    Script Date: 29/05/2019 10:20:56 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEventByID]
	@id INT
AS
BEGIN
SELECT * FROM dbo.Events WHERE @id=ID
END

GO
USE [master]
GO
ALTER DATABASE [EvensDB] SET  READ_WRITE 
GO
