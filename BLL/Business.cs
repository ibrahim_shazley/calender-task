﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;


namespace BLL
{
   public class Business
    {
        EvensDBEntities _Context;
        public Business()
        {
            _Context = new EvensDBEntities();
        }
        public bool AddEvent(List<Events> eventData)
        {
            if (eventData.Count!=0)
            {
                for (int i = 0; i < eventData.Count; i++)
                {

                    new EvensDBEntities().CNT.SqlQuery($"exec [dbo].[AddEvents]'{eventData[i].Title}','{eventData[i].startDate}','{eventData[i].startTime}','{eventData[i].endDate}','{eventData[i].endTime}'").ToList();


                }
                return true;
            }
          
            else
            return false;

        }
        public IEnumerable<EventDTO> GetAllEvents()
        {
            return new EvensDBEntities().Events.SqlQuery("exec [dbo].[GetAllEvents]").ToList().Select(x => new EventDTO
            {
                id=x.ID,
                title = x.Title,
                startDate =x.startDate,
                startTime = x.startTime,
                endDate = x.endDate,
                endTime=x.endTime
            });
        }

        public bool EditEvent(int ID, string title,string startDate, string startTime, string endDate, string endTime)
        {
            try
            {
                new EvensDBEntities().CNT.SqlQuery($"exec [dbo].[EditEvent]{ID},'{title}','{startDate}','{startTime}','{endDate}','{endTime}'").ToList();
                return true;
            }
            catch(Exception ex) { return false; }
        }

        public IEnumerable<EventDTO> GetEvent(int ID)
        {
            return new EvensDBEntities().Events.SqlQuery($"exec [dbo].[GetEventByID] {ID}").ToList().Select(x => new EventDTO
            {
                id = x.ID,
                title = x.Title,
                startDate = x.startDate,
                startTime = x.startTime,
                endDate = x.endDate,
                endTime = x.endTime
            });
        }

        public bool DeleteEvent(int ID)
        {
            try
            {
                new EvensDBEntities().CNT.SqlQuery($"exec [dbo].[DeleteEvent] {ID}").ToList();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
