﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="EditEvent.aspx.cs" Inherits="CalenderTask.EditEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="scripts/jquery-3.4.0.min.js"></script>
    <link href="scripts/esm/bootstrap.css" rel="stylesheet" />
    <script src="scripts/esm/bootstrap.js"></script>
    <link href="scripts/esm/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="scripts/esm/bootstrap-datetimepicker.js"></script>
   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
<div class="container">
        <div class="col">
            <div class="col-xs-4">
                <label>Title</label>
                <input type="text" id="Title" class="form-control" />
            </div>

            <br />

                <div class="col-xs-4">
                <label>Start</label>
                <input type="text" id="start" class="form-control" />
            </div>
            <br/>
                <div class="col-xs-4">
                <label>End</label>
                <input type="text" id="end" class="form-control" />
            </div>
            <br />
            <div class="col-xs-6">
                <button type="button" class="btn btn-primary" onclick="EditEvent()">Update Event</button>
                  <button type="button" class="btn btn-danger" onclick="DeleteEvent()">Delete Event</button>
            </div>

          
        </div>
    </div>

    
   <div class="form-group">
                <label for="dtp_input1" class="col-md-2 control-label">DateTime Picking</label>
                <div class="input-group date form_datetime col-md-5" data-date="2019-05-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">

                    <input class="form-control" size="16" type="text" value="" readonly="readonly"/>

                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value="" /><br />
            </div>

    <script>
        $(document).ready(function () {
            GetEvent();
        });


        function GetEvent() {
            jQuery.ajax({
                url: 'EditEvent.aspx/GetEvent',
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ ID: Number(window.location.href.split('?id=')[1]) }),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    debugger
                    $('#Title').val(JSON.parse(data.d)[0].title);
                    $('#start').val(JSON.parse(data.d)[0].start);
                    $('#end').val(JSON.parse(data.d)[0].end);

                }
            });
        }
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });

        //function EditEvent(ID) {
        //        jQuery.ajax({
        //            url: 'EditEvent.aspx/EditEvents',
        //            type: "POST",
        //            dataType: "json",
        //            data: JSON.stringify({ ID: Number(window.location.href.split('?id=')[1]), title: $('#Title').val(), start: $('#start').val(), end: $('#end').val() }),
        //            contentType: "application/json; charset=utf-8",
        //            success: function (data) {
        //                window.location.href = '/Calender.aspx';
        //            }
        //        });
        //    }
        
        function DeleteEvent(ID) {
            debugger
            jQuery.ajax({
                url: 'EditEvent.aspx/DeleteEvent',
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ ID: Number(window.location.href.split('?id=')[1]) }),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    window.location.href = '/Calender.aspx';
                }
            });
        }

       
    </script>


</asp:Content>
