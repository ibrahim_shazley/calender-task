﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using DAL;
using BLL;

namespace CalenderTask
{
    public partial class EditEvent : System.Web.UI.Page
    {
        public static Business bussiness = new Business();
        JsonSerializer serializer = new JsonSerializer();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static bool EditEvents(int ID, string title, string startDate, string startTime, string endDate, string endTime)
        {
            return bussiness.EditEvent(ID, title, startDate,startTime,endDate, endTime);
        }

        [WebMethod]
        public static string GetEvent(int ID)
        {
            return JsonConvert.SerializeObject(bussiness.GetEvent(ID));
        }

        [WebMethod]
        public static bool DeleteEvent(int ID)
        {
            return bussiness.DeleteEvent(ID);
        }

    }
}