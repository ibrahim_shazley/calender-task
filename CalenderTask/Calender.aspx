﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calender.aspx.cs" Inherits="CalenderTask.Calender" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="content-type" content="text/htm/l; charset=UTF-8" />
    
    <title>Calendar</title>
    <script src="scripts/jquery-3.3.1.js"></script>
    <link href="Content/jquery.weekcalendar.css" rel="stylesheet" />
     <link href="Content/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
    <script src="scripts/jquery-ui-1.8.4.custom.min.js"></script>
    <script src="scripts/daypilot-all.min.js"></script>
    <script src="scripts/jquery-ui.js"></script>
    <link href="Content/images/bootstrap.css" rel="stylesheet" />
    <script src="Content/images/bootstrap.js"></script>
   
  <style type='text/css'>
      body {
          font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
          margin: 0;
      }

      h1 {
          margin: 0 0 1em;
          padding: 0.5em;
      }

      p.description {
          font-size: 0.8em;
          padding: 1em;
          position: absolute;
          top: 3.2em;
          margin-right: 400px;
      }

      #message {
          font-size: 0.7em;
          position: absolute;
          top: 1em;
          right: 1em;
          width: 350px;
          display: none;
          padding: 1em;
          background: #ffc;
          border: 1px solid #dda;
      }
  </style>
  <script type="text/javascript" src="scripts/jquery-1.4.4.min.js"></script>
    <link href="Content/jquery.weekcalendar.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/jquery-ui-1.8.11.custom.min.js"></script>
    <script type="text/javascript" src="scripts/date.js"></script>

    <script src="scripts/jquery.weekcalendar.js"></script>
    <script type='text/javascript'>
        var year = new Date().getFullYear();
        var month = new Date().getMonth();
        var day = new Date().getDate();

        var SavedEvents = []
        var eventData = [];


        var time_start = false;
        var time_end = false;
        var ID = false;

        var response = false;
        $(document).ready(function () {
            loadEvents();
            
        });
     
           



        function cal() {
            $('#calendar').weekCalendar({
                timeslotsPerHour: 6,
                timeslotHeigh: 30,
                hourLine: true,
                data: SavedEvents,
                height: function ($calendar) {
                    return $(window).height() - $('h1').outerHeight(true);
                },
                eventRender: function (calEvent, $event) {
                    if (calEvent.end.getTime() < new Date().getTime()) {

                        $event.css('backgroundColor', '#aaa');
                        $event.find('.time').css({ 'backgroundColor': '#999', 'border': '1px solid #888' });
                    }
                },

                eventNew: function (calEvent, $event) {
                    
                    $('#btnAdd').click();
                    response = $('#AddTitle').val();
                    calEvent.title = response;
                    $('#AddStart').val(new Date(calEvent.start).toISOString().split("T")[0]);
                    $('#AddStarttime').val(new Date(calEvent.start).toString("H:mm"));
                    $('#AddEnd').val(new Date(calEvent.end).toISOString().split("T")[0]);
                    $('#AddEndtime').val(new Date(calEvent.end).toString("H:mm"));
                    time_start = $('#AddStart').val();
                    time_end = $('#AddEnd').val();
                
                },


                eventDrop: function (calEvent, $event) {
                    $('#btnEdit').click();
                    time_start = calEvent.start;
                    time_end = calEvent.end;
                    ID = calEvent.id;
                    $('#EditTitle').val(calEvent.title);
                    $('#EditStart').val(new Date(calEvent.start).toISOString().split("T")[0]);
                    $('#EditStarttime').val(new Date(calEvent.start).toString("H:mm"));
                    $('#EditEnd').val(new Date(calEvent.end).toISOString().split("T")[0]);
                    $('#EditEndtime').val(new Date(calEvent.end).toString("H:mm"));

                    displayMessage('<strong>Moved Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);
                },
                eventResize: function (calEvent, $event) {
                    $('#btnEdit').click();
                    time_start = calEvent.start;
                    time_end = calEvent.end;
                    ID = calEvent.id;
                    $('#EditTitle').val(calEvent.title);
                    $('#EditStart').val(new Date(calEvent.start).toISOString().split("T")[0]);
                    $('#EditStarttime').val(new Date(calEvent.start).toString("H:mm"));
                    $('#EditEnd').val(new Date(calEvent.end).toISOString().split("T")[0]);
                    $('#EditEndtime').val(new Date(calEvent.end).toString("H:mm"));

                   displayMessage('<strong>Resized Event</strong><br/>Start: ' + calEvent.start + '<br/>End: ' + calEvent.end);

                },
                

                eventClick: function (calEvent, $event) {
                    $('#btnEdit').click();
                    ID = calEvent.id;
                    $('#EditTitle').val(calEvent.title);
                    $('#EditStart').val(new Date(calEvent.start).toISOString().split("T")[0]);
                    $('#EditStarttime').val(new Date(calEvent.start).toString("H:mm"));
                    $('#EditEnd').val(new Date(calEvent.end).toISOString().split("T")[0]);
                    $('#EditEndtime').val(new Date(calEvent.end).toString("H:mm"));



                    //  window.location.href='/EditEvent.aspx?id=' + calEvent.id + ''
                },
                eventMouseover: function (calEvent, $event) {
                    displayMessage('<strong>Mouseover Event</strong><br/>Start: ' + new Date(calEvent.start).toString("MM/dd/yyyy hh:mm tt") + '<br/>End: ' + new Date(calEvent.end).toString("MM/dd/yyyy hh:mm tt") + '<br/>title: ' + calEvent.title );
                },

                noEvents: function () {
                    displayMessage('There are no events for this week');
                }

            });

            function displayMessage(message) {
                $('#message').html(message).fadeIn();
            }

            $('<div id="message" class="ui-corner-all"></div>').prependTo($('body'));

        }


      


        function SaveEvents() {
            debugger;
            if ($('#AddTitle').val() != "") {
                if ($('#AddStarttime').val() < $('#AddEndtime').val() && $('#AddStart').val() <= $('#AddEnd').val()) {

                    jQuery.ajax({
                        url: 'Calender.aspx/AddEvent',
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ eventData: eventData }),
                        success: function (data) {
                            // location.reload();
                            $('#btnAddClose').click();
                            // $('#calendar').html("");
                            eventData = [];
                            loadEvents();
                            //   $("#calendar").html("");
                          $("#body").load(location.href + "#body");
                           

                          
                        
                            
                      
                            
                       

                        }
                    });
                }
                else {alert("Start Time Must Be Less Than End Time") }
            }
            else { alert("Please Add Event Title") }
          

        }


        function AddEvent() {
           
            var startyear = new Date($('#AddStart').val()).getFullYear();
            var startmonth = new Date($('#AddStart').val()).getMonth();
            var startday = new Date($('#AddStart').val()).getDate();
            var starthour = $('#AddStarttime').val().split(":")[0];
            var startminute = $('#AddStarttime').val().split(":")[1];

            var Endyear = new Date($('#AddEnd').val()).getFullYear();
            var Endtmonth = new Date($('#AddEnd').val()).getMonth();
            var Endtday = new Date($('#AddEnd').val()).getDate();
            var Endthour = $('#AddEndtime').val().split(":")[0];
            var Endminute = $('#AddEndtime').val().split(":")[1];
            //'start': new Date(startyear, startmonth, startday, starthour, startminute).toString(),

            eventData.push({
                'startDate': $('#AddStart').val(),
                'startTime': $('#AddStarttime').val(),
                'endDate': $('#AddEnd').val(),
                'endTime': $('#AddEndtime').val(),
                'title': $('#AddTitle').val()
            });
            console.log(eventData);
            console.log(eventData.length);
           
            SaveEvents();
        }
        

        function loadEvents() {
          
            var startyear = false;
            var startmonth = false;
            var startday = false;
            var starthour = false;
            var startminute = false;

            var endyear = false;
            var endmonth = false;
            var endday = false;
            var endhour = false;
            var endminute = false;
            jQuery.ajax({
                url: 'Calender.aspx/GetAllEvents',
                type: "POST",
                dataType: "json",
                data: "",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    let items = JSON.parse(data.d);
                    console.log(items);
                    for (var i = 0; i < items.length; i++) {
                        startyear = new Date(items[i].startDate).getFullYear();
                        startmonth = new Date(items[i].startDate).getMonth();
                        startday = new Date(items[i].startDate).getDate();
                        starthour = (items[i].startTime).split(":")[0];
                        startminute = (items[i].startTime).split(":")[1];

                        endyear = new Date(items[i].endDate).getFullYear();
                        endmonth = new Date(items[i].endDate).getMonth();
                        endday = new Date(items[i].endDate).getDate();
                        endhour = (items[i].endTime).split(":")[0];
                        endminute = (items[i].endTime).split(":")[1];
                        SavedEvents.push({
                            'id': items[i].id, 'start': new Date(startyear, startmonth, startday, starthour, startminute),
                            'end': new Date(endyear, endmonth, endday, endhour, endminute), 'title': items[i].title
                        });
                    }
                    //call the drawing funtion after loading the data from the the database
                    cal();
                    SavedEvents = [];
                   
                }
            })
        }
        function EditEvent(ID) {
            if ($('#EditTitle').val() != "") {
                if ($('#EditStarttime').val() < $('#EditEndtime').val() && $('#EditStart').val() <= $('#EditEnd').val()) {
                    jQuery.ajax({
                        url: 'EditEvent.aspx/EditEvents',
                        type: "POST",
                        dataType: "json",
                        data: JSON.stringify({ ID: ID, title: $('#EditTitle').val(), startDate: $('#EditStart').val(), startTime: $('#EditStarttime').val(), endDate: $('#EditEnd').val(), endTime: $('#EditEndtime').val() }),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $("#body").load(location.href + "#body");
                        }
                    });
                }
                else {alert("Srart time must be less than end time")}
            } 
            else {alert("Please Add Event Title")}
        
        }

        function DeleteEvent(ID) {
            debugger
            jQuery.ajax({
                url: 'EditEvent.aspx/DeleteEvent',
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ ID: ID }),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("#body").load(location.href + "#calendar");
                }
            });
        }

      
      
    </script>
</head>
<body id="body">
 
    <h1>Week Calendar </h1>

    <%--edit event modal--%>
    <button type="button" id="btnEdit" data-toggle="modal" data-target="#Modal" hidden="hidden" data-whatever="@mdo"></button>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
 <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <br />
          <div class="form-group">
            <label for="Title" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="EditTitle"  style="width:100%"/>
          </div>
         

            <div class="form-group">
            <label for="start" class="col-form-label">Start</label>
        Date &nbsp;<input type="date" class="form-control" id="EditStart"  required="required" style="width:40%" />
        Time &nbsp;<input type="time" class="form-control" id="EditStarttime"  required="required" style="width:35%" />
          </div>

            
            <div class="form-group">
            <label for="End" class="col-form-label">End</label>
          Date &nbsp;<input type="date" class="form-control" id="EditEnd" required="required"style="width:40%"/>
          Time &nbsp;<input type="time" class="form-control" id="EditEndtime" required="required"style="width:35%"/>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnEditClose" class="btn btn-secondary" data-dismiss="modal">Close</button>

        <button type="button" id="UpdateEvent" onclick="EditEvent(ID)" class="btn btn-primary">Update Event</button>

         <button type="button" id="DeleteEvent" onclick="DeleteEvent(ID)" class="btn btn-danger">Delete Event</button>
      </div>
    </div>
  </div>
</div>



  <%--  add event maodal --%>
    
    
    <button type="button" id="btnAdd" data-toggle="modal" data-target="#exampleModal1" hidden="hidden" data-whatever="@mdo"></button>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
 <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Add Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="Title" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="AddTitle" style="width:100%"/>
          </div>
         

            <div class="form-group">
            <label for="start" class="col-form-label">Start</label>
          Date &nbsp;<input type="date" class="form-control" id="AddStart"  required="required" style="width:40%" /> 
          Time &nbsp;<input type="time" class="form-control" id="AddStarttime"  required="required" style="width:35%" />

          </div>
            <div class="form-group">
            <label for="End" class="col-form-label">End</label>
          Date &nbsp;<input type="date" class="form-control" id="AddEnd" required="required"style="width:40%"/>
          Time &nbsp;<input type="time" class="form-control" id="AddEndtime" required="required"style="width:35%"/>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnAddClose" class="btn btn-secondary" data-dismiss="modal">Close</button>

        <button type="button" id="AddE" onclick=" AddEvent()"  class="btn btn-primary">Add Event</button>

      </div>
    </div>
  </div>
</div>





    <%--<button type="button" id="AddEvents" class="btn btn-danger" value="SaveEvents" onclick="SaveEvents()"  style="float:left;margin-left:20px;padding-left:20px;" >Save All Events</button>
            <button type="button" id="Save" class="btn btn-danger" value="AddEvent" onclick="AddEvent()" style="float:left;margin-left:90px;padding-left:20px;" >Add single Event</button>--%>
    <br />
    
    <div id='calendar'></div>
 
       
</body>
</html>
