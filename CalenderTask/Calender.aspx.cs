﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using DAL;
using BLL;


namespace CalenderTask
{
    public partial class Calender : System.Web.UI.Page
    {
        public static Business bussiness = new Business();
        JsonSerializer serializer = new JsonSerializer();


        protected void Page_Load(object sender, EventArgs e)
        {
      

        }

        [WebMethod]
        public static bool AddEvent(List<Events> eventData)
        {
            return bussiness.AddEvent(eventData);
        }
        [WebMethod]
        public static string GetAllEvents()
        {
            return JsonConvert.SerializeObject(bussiness.GetAllEvents());
        }
       
    }
}